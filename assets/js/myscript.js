
const status = res => (res.status !== 200) ?
    Promise.reject(new Error(res.statusText)) :
    Promise.resolve(res);
const myFetch = (url, data, method) => fetch( url, {
    method: typeof method !== "undefined" ? method : "POST",
    body: data
}).then(status)

if(typeof Vue === "function"){
    var appVue = new Vue({
        el: "#experimentalApp",
        data: {
            menu: {},
            page: {},
            setting: {},
        },
        methods: {
            myClick: _.debounce(function(e) {
                console.log("log");
            }),
        }
    });
}