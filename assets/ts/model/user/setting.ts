interface Setting {
    token: string;
    server: string;
}

class Setting {
    token: string;
    server: string = 'https://localhost:8080';

    constructor(token: string) {
        this.token = token;
    }
}