interface User {
    name: string;
    setting: Setting;
}

class User {
    name: string;
    setting: Setting;

    constructor(name: string, token: string) {
        this.name = name;
        this.setting = new Setting(token);
    }
}